## __Objetivo__

O objetivo deste documento é descrever os serviços a serem utilizados na proposta de arquitetura inicial

Os serviços a serem descritos serão:
Azure Data Factory (ADF)
Azure Synapse Analytics
Azure Data Lake Storage Gen2


## __Proposta inicial__

![Artifact Registry](../../../assets//engenharia/arquitetura/arquitetura_gcp/arquitetura_inicial.png)

## __Serviços a serem Utilizados__
Azure Data Factory (ADF)<p>
plataforma como serviço (PaaS) do Microsoft Azure que oferece movimentação e transformação de dados. Ele suporta a movimentação de dados entre muitas fontes de dados locais e na nuvem

Azure SQL<p>
Obtenha uma experiência unificada em todo o seu portfólio SQL e uma gama completa de opções de implantação, da borda à nuvem. Os serviços de banco de dados SQL do Azure seguros e inteligentes facilitam as principais operações com a linguagem de forma prática e dinâmica

Azure Machine Learning<p>
O Azure Machine Learning capacita cientistas de dados e desenvolvedores a criar, implantar e gerenciar modelos de alta qualidade com mais rapidez e confiança. Ele acelera o tempo de retorno com mLOps (operações de aprendizado de máquina) líderes do setor, interoperabilidade de código aberto e ferramentas integradas. Essa plataforma confiável foi projetada para aplicativos de IA responsável no aprendizado de máquina

Azure Data Lake Storage Gen2<p>
O Azure Data Lake Storage Gen2 é um conjunto de funcionalidades dedicadas à análise de Big Data, criado no Armazenamento de Blobs do Azure



## __Fluxo de Dados__

Fluxo 1 

![Artifact Registry](../../../assets//engenharia/arquitetura/arquitetura_gcp/Proposta Cloud v1-Página-2.drawio.png)

Esse fluxo ocorre através de um repositório Azure disponibilizado pela Iveco. Essa movimentação para o Data Lake do projeto Carga pesada é feito através da ferramenta Data Factory e os arquivos são retirados desse repositório intermediário para facilitar nosso acesso aos dados.

Fluxo 2

![Artifact Registry](../../../assets//engenharia/arquitetura/arquitetura_gcp/Proposta Cloud v1-Página-3.drawio.png)

Nessa parte do processo de ETL faremos a movimentação dos dados do Data Lake para as ferramentas de análise e processamento: Azure Machine Learning e Azure SQL para seus devidos tratamentos e transformações.

Fluxo 3

![Artifact Registry](../../../assets//engenharia/arquitetura/arquitetura_gcp/Proposta Cloud v1-Página-4.drawio.png)

E por fim, os dados estarão disponíveis para consulta, geração de relatórios e ferramentas de service BI. Os mesmos dados ainda alimentarão o principal produto entregue para a área de negócio: o modelo de ML.