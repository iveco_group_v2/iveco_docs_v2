## __Tabelas__

<center>


# Tabela Códigos de falhas 

|Colunas   	| Informação	|
|---	|---	|
|2 DIGITOS   	| Componentes e serviços que são prestados para o veículo    	|
|4 DIGITOS   	|Os dois primeiros dígitos se referem a onde foi a falha exemplo<p>5404 <p>54-motor<p>04-bloco<p>5404-MOTOR/BLOCO |
|6 DIGITOS   	|Basicamente é a junção das colunas de 2 dígitos com a de 4 dígitos<p>sendo sempre os 2 primeiros dígitos indicando onde foi o problema<p>exemplo<p>540420<p>54-motor<p>0420-CAMISAS CILINDRO|

# Abaco Details Daily - Contract Only
|Colunas|Informação                   |
|-------|-----------------------------|
|Contract Number  |Número referente ao contrato do cliente   |
|Sales Organization |organização de onde foi feita a venda sendo ela: AR01-Argentina BR01-Brasil   |
|Creation Date of Contract |Data de criação do contrato  |
|Material Number |Número do material que foi utilizado no serviço  |
|Master Service Description  |Descrição do serviço principal( geralmente aqui ele tá apontando a empresa que fez o serviço ou o evento que aconteceu)   |
|Warranty Start Date |Data de início da garantia do contrato   |
|Start Date |Data de início do contrato   |
|End Date | Data final do contrato(encerramento)  |
|Contract Period|Período do contrato em meses |
|Percentage of Life |Porcentagem de vida do contrato (periodo Cumprido)|
|Annual KM |Quilometragem anual          |
|Initial Km |Quilometragem  inicial       |
|Km Contracted  |Quilometragem contratada     |
|Suspension km| quilometragem de suspensão  |
|Date of suspension  |data da suspensão            |
|Reactivation Km|Reativação da quilometragem  |
|Reactivation date|data de reativação da suspensão  |
|Vehicle Identification Number (VIN)|Número de identificação do veículo(chassi)|
|Customer|Código do cliente            |
|Customer Name|nome do cliente ou evento ocorrido |
|Dealer Code|código do vendedor           |
|Dealer Name|Nome do vendedor(a quem foi prestado o serviço) |
| Billing Block in SD Document  | Bloqueio de cobrança no documento SD  |
| Message Number  | Número da mensagem(dd)      |
| Actual Economic Income  | Custo do contrato desde inicio ate a data do relatório|
|Actual Economic Cost (30-70-80)  | Custo do contrato desde inicio ate a data do relatório|
| Actual Economic Balance (30-70-80)| Balanço (Diferenca entre receita e custo) do contrato desde o inicio ate a data do relatorio|
| Income Sold at end Life| 	Projeção de receita que iremos receber ate o fim do contrato|
| Cost at end Life gross (30-70-80)| Projeção de custo do contrato no seu final de vida|
| Balance at end Life (30-70-80| Projecao de Balanco (Diferenca entre receita e custo) do contrato no seu final de vida|
| Technical Price End Life| preço da parte tecnica ao fim do contrato |
| Balance at end Life (30-70-80)| Projecao de Balanco (Diferenca entre receita e custo) do contrato no seu final de vida|


</center>

## __Ajuda__

!!! danger ""
    :x: &nbsp; **[Erro no deploy](../ajuda/erro_no_deploy.md)** 
